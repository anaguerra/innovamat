<?php

declare(strict_types=1);

namespace App\Entity;

use Carbon\Carbon;
use DateTime;

class Answer
{
    public const MIN_SCORE_APPROVE_ACTIVITY = 50;

    public int $studentId;
    public string $activityIdentifier;
    public int $score;
    public int $time;
    public string $solution;
    public Carbon $createdAt;

    public function __construct(
        int      $studentId,
        string   $activityIdentifier,
        int      $score,
        int      $time,
        string   $solution
    ) {
        $this->studentId = $studentId;
        $this->activityIdentifier = $activityIdentifier;
        $this->score = $score;
        $this->time = $time;
        $this->solution = $solution;
        $this->createdAt = Carbon::now();
    }


    public static function createFromAssocArray(array $data): self
    {
        return new self(
            $data['student_id'],
            $data['activity_identifier'],
            $data['score'],
            $data['time'],
            $data['solution']
        );
    }

    public function getStudentId(): int
    {
        return $this->studentId;
    }

    public function getActivityIdentifier(): string
    {
        return $this->activityIdentifier;
    }

    public function getScore(): int
    {
        return $this->score;
    }

    public function getTime(): int
    {
        return $this->time;
    }

    public function getSolution(): string
    {
        return $this->solution;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }
}
