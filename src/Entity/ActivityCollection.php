<?php

declare(strict_types=1);

namespace App\Entity;

use ArrayIterator;

class ActivityCollection extends ObjectCollection
{
    public static function allowedObjectClass(): string
    {
        return Activity::class;
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->items);
    }

    protected function itemAssertions($item): void
    {
    }

    public function toArray(): array
    {
        return array_map(
            fn (Activity $activity) => $activity->toArray(),
            $this->items
        );
    }
}
