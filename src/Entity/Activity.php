<?php

declare(strict_types=1);

namespace App\Entity;

class Activity
{
    public const DIFFICULTY_MIN_VALUE = 0;
    public const DIFFICULTY_MAX_VALUE = 10;

    private string $identifier;
    private string $name;
    private int $position;
    private int $difficulty;
    private int $time;
    private string $solution;
    private int $itinerary;

    public function __construct(
        string $identifier,
        string $name,
        int $position,
        int $difficulty,
        int $time,
        string $solution,
        int $itinerary
    ) {
        $this->identifier = $identifier;
        $this->name = $name;
        $this->position = $position;
        $this->difficulty = $difficulty;
        $this->time = $time;
        $this->solution = $solution;
        $this->itinerary = $itinerary;
        $this->assertValidDifficulty();
    }

    public static function createFromAssocArray(array $data): self
    {
        return new self(
            $data['identifier'],
            $data['name'],
            $data['position'],
            $data['difficulty'],
            $data['time'],
            $data['solution'],
            $data['itinerary']
        );
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDifficulty(): int
    {
        return $this->difficulty;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getTime(): int
    {
        return $this->time;
    }

    public function getSolution(): string
    {
        return $this->solution;
    }

    public function getItinerary(): int
    {
        return $this->itinerary;
    }

    public function toArray(): array
    {
        return [
            'identifier' => $this->identifier,
            'name' => $this->name,
            'position' => $this->position,
            'difficulty' => $this->difficulty,
            'time' => $this->time,
            'solution' => $this->solution
        ];
    }

    private function assertValidDifficulty(): void
    {
        if ($this->difficulty < self::DIFFICULTY_MIN_VALUE
            || $this->difficulty > self::DIFFICULTY_MAX_VALUE) {
            throw new \Exception(sprintf('Invalid value of difficulty "%s"', $this->difficulty));
        }
    }
}
