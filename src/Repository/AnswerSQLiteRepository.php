<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Activity;
use App\Entity\Answer;
use App\Entity\ActivityCollection;

class AnswerSQLiteRepository extends Sqlite3Repository implements AnswerRepository
{
    public function create(Answer $answer): void
    {
        $sql = 'INSERT INTO answer (student_id, activity_identifier, score, time, solution, created_at) 
                    VALUES (:student_id, :activity_identifier, :score, :time, :solution, :created_at)';

        $stmt = $this->db->prepare($sql);

        $stmt->bindValue(':student_id', $answer->getStudentId(), SQLITE3_INTEGER);
        $stmt->bindValue(':activity_identifier', $answer->getActivityIdentifier(), SQLITE3_TEXT);
        $stmt->bindValue(':score', $answer->getScore(), SQLITE3_INTEGER);
        $stmt->bindValue(':time', $answer->getTime(), SQLITE3_INTEGER);
        $stmt->bindValue(':solution', $answer->getSolution(), SQLITE3_TEXT);
        $stmt->bindValue(':created_at', $answer->getCreatedAt()->format('Y-m-d H:i:s'), SQLITE3_TEXT);

        $stmt->execute();
    }
}
