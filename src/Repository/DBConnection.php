<?php

declare(strict_types=1);

namespace App\Repository;

use SQLite3;

class DBConnection
{
    private static $instance = null;

    private SQLite3 $connection;

    public function __construct()
    {
        $this->connection = new SQLite3('../innovamat.sqlite');
    }

    public static function getInstance(): self
    {
        if (is_null(self::$instance)) {
            self::$instance = new DBConnection();
        }

        return self::$instance;
    }

    public function getConnection(): SQLite3
    {
        return $this->connection;
    }
}
