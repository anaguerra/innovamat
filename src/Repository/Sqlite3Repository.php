<?php

declare(strict_types=1);

namespace App\Repository;

class Sqlite3Repository
{
    protected \SQLite3 $db;

    public function __construct()
    {
        $this->db = DBConnection::getInstance()->getConnection();
    }
}
