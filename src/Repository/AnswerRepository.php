<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Answer;

interface AnswerRepository
{
    public function create(Answer $answer): void;
}
