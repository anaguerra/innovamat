<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Activity;
use App\Entity\ActivityCollection;

interface ActivityRepository
{
    public function findByItinerary(int $itineraryId): ActivityCollection;

    public function findByIdentifier(string $identifier): ?Activity;

    public function createOrFail(Activity $activity): void;

    public function getLastAnsweredByItineraryAndStudent(int $itineraryId, int $studentId): ?Activity;

    public function findByItineraryAndPosition(int $itineraryId, int $position): ?Activity;
}
