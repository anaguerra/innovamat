<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Activity;
use App\Entity\ActivityCollection;
use App\Entity\Answer;

class ActivitySQLiteRepository extends Sqlite3Repository implements ActivityRepository
{
    public function findByItinerary(int $itineraryId): ActivityCollection
    {
        $sql = 'SELECT * FROM activity WHERE itinerary = :itinerary order by difficulty, position';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':itinerary', $itineraryId);
        $result = $stmt->execute();

        $activities = new ActivityCollection();

        while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
            $activities->add(Activity::createFromAssocArray($row));
        }

        return $activities;
    }

    public function findByIdentifier(string $identifier): ?Activity
    {
        $sql = 'SELECT * FROM activity where identifier = :identifier';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':identifier', $identifier);
        $result = $stmt->execute();

        $row = $result->fetchArray(SQLITE3_ASSOC);

        // This also could be a NotFoundException
        if (false === $row) {
            return null;
        }

        return Activity::createFromAssocArray($row);
    }

    public function createOrFail(Activity $activity): void
    {
        $sql = 'INSERT INTO activity (identifier, name, position, difficulty, time, solution, itinerary) 
                    VALUES (:identifier, :name, :position,  :difficulty, :time, :solution, :itinerary)';

        $stmt = $this->db->prepare($sql);

        $stmt->bindValue(':identifier', $activity->getIdentifier(), SQLITE3_TEXT);
        $stmt->bindValue(':name', $activity->getName(), SQLITE3_TEXT);
        $stmt->bindValue(':position', $activity->getPosition(), SQLITE3_INTEGER);
        $stmt->bindValue(':difficulty', $activity->getDifficulty(), SQLITE3_INTEGER);
        $stmt->bindValue(':time', $activity->getTime(), SQLITE3_INTEGER);
        $stmt->bindValue(':solution', $activity->getSolution(), SQLITE3_TEXT);
        $stmt->bindValue(':itinerary', $activity->getItinerary(), SQLITE3_INTEGER);

        $stmt->execute();
    }


    public function getLastAnsweredByItineraryAndStudent(int $itineraryId, int $studentId): ?Activity
    {
        $sql = 'SELECT max(position), activity.* FROM activity
            left join answer on answer.activity_identifier = activity.identifier
                where student_id = :student_id and itinerary = :itinerary
                    and answer.score > ' . Answer::MIN_SCORE_APPROVE_ACTIVITY;

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':itinerary', $itineraryId);
        $stmt->bindParam(':student_id', $studentId);
        $result = $stmt->execute();

        $row = $result->fetchArray(SQLITE3_ASSOC);

        if (false === $row) {
            return null;
        }

        return Activity::createFromAssocArray($row);
    }

    public function findByItineraryAndPosition(int $itineraryId, int $position): ?Activity
    {
        $sql = 'SELECT * FROM activity WHERE itinerary = :itinerary and position =:position LIMIT 1';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':itinerary', $itineraryId);
        $stmt->bindParam(':position', $position);
        $result = $stmt->execute();

        $row = $result->fetchArray(SQLITE3_ASSOC);


        if (false === $row) {
            return null;
        }

        return Activity::createFromAssocArray($row);
    }
}
