<?php

declare(strict_types=1);

namespace App\Controller;

use App\Command\CreateActivityCommand;
use App\Command\CreateActivityHandler;
use App\Command\RegisterActivityCommand;
use App\Command\RegisterActivityHandler;
use App\Repository\ActivityRepository;
use App\Repository\AnswerRepository;
use App\Service\AnswerScoreCalculator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("activities/itinerary/{itinerary}", methods={"GET"})
     */
    public function getActivities(int $itinerary, Request $request, ActivityRepository $activityRepository): JsonResponse
    {
        $itinerary = $request->get('itinerary');
        $activities = $activityRepository->findByItinerary((int)$itinerary);

        return new JsonResponse($activities->toArray());
    }

    /**
     * @Route("activities/itinerary/{itinerary}", methods={"POST"})
     */
    public function postActivities(int $itinerary, Request $request, ActivityRepository $activityRepository): JsonResponse
    {
        $params = json_decode($request->getContent());

        $command = new CreateActivityCommand(
            $params->identifier,
            $params->name,
            $params->position,
            $params->difficulty,
            $params->time,
            $params->solution,
            $itinerary
        );

        // TODO: command bus...
        (new CreateActivityHandler($activityRepository))->handle($command);

        return new JsonResponse();
    }

    /**
     * @Route("/registerActivity/student/{student_id}", methods={"POST"})
     */
    public function registerActivity(
        int $student_id,
        Request  $request,
        ActivityRepository $activityRepository,
        AnswerRepository   $answerRepository,
        AnswerScoreCalculator  $scoreCalculator
    ): JsonResponse {
        $params = json_decode($request->getContent());

        $command = new RegisterActivityCommand(
            $params->identifier,
            $params->answers,
            $params->time,
            $student_id
        );

        // TODO: command bus...
        (new RegisterActivityHandler($activityRepository, $answerRepository, $scoreCalculator))->handle($command);

        return new JsonResponse();
    }

    /**
     * @Route("/nextActivity/itinerary/{itinerary}/student/{student_id}", methods={"GET"})
     */
    public function nextActivity(int $itinerary, int $student_id, ActivityRepository $activityRepository): JsonResponse
    {
        $lastActivity = $activityRepository->getLastAnsweredByItineraryAndStudent($itinerary, $student_id);
        $nextActivity = $activityRepository->findByItineraryAndPosition($itinerary, $lastActivity->getPosition()+1);

        return new JsonResponse([$nextActivity ? $nextActivity->getIdentifier() : 'Itinerario completado']);
    }
}
