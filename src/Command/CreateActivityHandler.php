<?php

namespace App\Command;

use App\Entity\Activity;
use App\Repository\ActivityRepository;

class CreateActivityHandler
{
    private ActivityRepository $activityRepository;

    public function __construct(ActivityRepository $activityRepository)
    {
        $this->activityRepository = $activityRepository;
    }

    public function handle(CreateActivityCommand $command): void
    {
        $activity = new Activity(
            $command->getIdentifier(),
            $command->getName(),
            $command->getPosition(),
            $command->getDifficulty(),
            $command->getTime(),
            $command->getSolution(),
            $command->getItinerary(),
        );

        $this->activityRepository->createOrFail($activity);
    }
}
