<?php

namespace App\Command;

class CreateActivityCommand
{
    private string $identifier;
    private string $name;
    private int $difficulty;
    private int $position;
    private int $time;
    private string $solution;
    private int $itinerary;

    public function __construct(
        string $identifier,
        string $name,
        int $position,
        int $difficulty,
        int $time,
        string $solution,
        int $itinerary
    ) {
        $this->identifier = $identifier;
        $this->name = $name;
        $this->position = $position;
        $this->difficulty = $difficulty;
        $this->time = $time;
        $this->solution = $solution;
        $this->itinerary = $itinerary;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDifficulty(): int
    {
        return $this->difficulty;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function getTime(): int
    {
        return $this->time;
    }

    public function getSolution(): string
    {
        return $this->solution;
    }

    public function getItinerary(): int
    {
        return $this->itinerary;
    }
}
