<?php

declare(strict_types=1);

namespace App\Command;

class RegisterActivityCommand
{
    private string $identifier;
    private string $answers;
    private int $time;
    private int $studentId;

    public function __construct(
        string $identifier,
        string $answers,
        int $time,
        int $studentId
    ) {
        $this->identifier = $identifier;
        $this->answers = $answers;
        $this->time = $time;
        $this->studentId = $studentId;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getAnswers(): string
    {
        return $this->answers;
    }

    public function getTime(): int
    {
        return $this->time;
    }

    public function getStudentId(): int
    {
        return $this->studentId;
    }
}
