<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Answer;
use App\Repository\ActivityRepository;
use App\Repository\AnswerRepository;
use App\Service\AnswerScoreCalculator;
use Exception;

class RegisterActivityHandler
{
    private ActivityRepository $activityRepository;
    private AnswerScoreCalculator $scoreCalculator;
    private AnswerRepository $answerRepository;

    public function __construct(
        ActivityRepository $activityRepository,
        AnswerRepository   $answerRepository,
        AnswerScoreCalculator    $scoreCalculator
    ) {
        $this->activityRepository = $activityRepository;
        $this->answerRepository = $answerRepository;
        $this->scoreCalculator = $scoreCalculator;
    }

    public function handle(RegisterActivityCommand $command): void
    {
        $activity = $this->activityRepository->findByIdentifier($command->getIdentifier());

        $this->assertValidSolution($command->getAnswers(), $activity->getSolution());

        $score = $this->scoreCalculator->calculate($command->getAnswers(), $activity->getSolution());

        $answer = new Answer(
            $command->getStudentId(),
            $command->getIdentifier(),
            $score,
            $command->getTime(),
            $command->getAnswers()
        );

        $this->answerRepository->create($answer);
    }


    private function assertValidSolution(string $sendedSolution, string $activitySolution): void
    {
        $studentSolutions = explode('_', $sendedSolution);
        $activitySolutions = explode('_', $activitySolution);

        if (count($studentSolutions) !== count($activitySolutions)) {
            throw new Exception(sprintf(
                'Given answers "%s" does not match with solution "%s"',
                $sendedSolution,
                $activitySolution
            ));
        }
    }
}
