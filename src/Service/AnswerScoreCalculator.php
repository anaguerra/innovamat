<?php

declare(strict_types=1);

namespace App\Service;

class AnswerScoreCalculator
{
    public function calculate(string $sendedSolution, string $activitySolution): int
    {
        $activityAnswers =  explode('_', $activitySolution);
        $studentAnswers = explode('_', $sendedSolution);

        /*
        * To obtain the score we first calculate each answer's value
        * and then add it to the final score for every correct one
        */
        $score = 0;
        $isCorrect = 100 / count($activityAnswers);
        foreach ($studentAnswers as $index => $answer) {
            $solution = $activityAnswers[$index];
            if ($solution === $answer) {
                $score += $isCorrect;
            }
        }

        return (int)$score;
    }
}
