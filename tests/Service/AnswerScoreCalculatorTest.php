<?php

namespace App\Tests\Service;

use App\Service\AnswerScoreCalculator;
use App\Tests\FakeBuilder\ActivityFakeBuilder;
use PHPUnit\Framework\TestCase;

class AnswerScoreCalculatorTest extends TestCase
{

    private AnswerScoreCalculator $calculator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->calculator = new AnswerScoreCalculator();
    }

    /**
     * @dataProvider provider
     */
    public function testCalculateScore(string $sendedSolution, string $activitySolution, int $expectedScore): void
    {
        $this->assertEquals($expectedScore, $this->calculator->calculate($sendedSolution, $activitySolution));
    }

    public function provider(): array
    {
        return [
            ['1_0_2_-4_9', '1_0_2_-5_9', 80],
            ['1_0_2', '1_0_2', 100],
            ['1_0', '1_1', 50]
        ];
    }
}
