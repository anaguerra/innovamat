<?php
declare(strict_types=1);


namespace App\Tests\FakeBuilder;


use App\Entity\Activity;

class ActivityFakeBuilder extends FakeBuilder
{
    public string $identifier;
    public string $name;
    public int $position;
    public int $difficulty;
    public int $time;
    public string $solution;
    public int $itinerary;


    public function withIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function withPosition(int $position): self
    {
        $this->position = $position;
        return $this;
    }

    public function withSolution(string $solution): self
    {
        $this->solution = $solution;
        return $this;
    }

    public function withName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function withDifficulty(int $difficulty): self
    {
        $this->difficulty = $difficulty;
        return $this;
    }

    public function withTime(int $time): self
    {
        $this->time = $time;
        return $this;
    }

    public function withItinerary(int $itinerary): self
    {
        $this->itinerary = $itinerary;
        return $this;
    }


    public function random(): self
    {
        $this->identifier = $this->getFaker()->unique()->lexify('??');
        $this->name = $this->getFaker()->word();
        $this->position = $this->getFaker()->numberBetween(1,6);
        $this->difficulty = $this->getFaker()->numberBetween(1,10);
        $this->time = $this->getFaker()->numberBetween(60, 160);
        $this->solution = $this->getFaker()->word();
        $this->itinerary = $this->getFaker()->numberBetween(1,2);
        return $this;
    }

    public function generate(): Activity
    {
        return new Activity(
            $this->identifier,
            $this->name,
            $this->position,
            $this->difficulty,
            $this->time,
            $this->solution,
            $this->itinerary
        );
    }
}
