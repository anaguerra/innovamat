<?php declare(strict_types=1);

namespace App\Tests\FakeBuilder;

use Faker\Factory;
use Faker\Generator;

abstract class FakeBuilder
{
    protected static ?Generator $faker = null;

    public function __construct()
    {
        $this->random();
    }

    abstract public function random(): self;

    public function getFaker():Generator
    {
        if (self::$faker === null) {
            self::$faker = Factory::create();
        }
        return self::$faker;
    }
}
