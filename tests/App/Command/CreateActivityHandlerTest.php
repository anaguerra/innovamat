<?php

namespace App\Command;

use App\Repository\ActivityRepository;
use App\Tests\FakeBuilder\ActivityFakeBuilder;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CreateActivityHandlerTest extends TestCase
{
    private MockObject $activityRepository;
    private ActivityFakeBuilder $activityFakeBuilder;
    private CreateActivityHandler $handler;

    protected function setUp(): void
    {
        parent::setUp();
        $this->activityFakeBuilder = new ActivityFakeBuilder();
        $this->activityRepository = $this->createMock(ActivityRepository::class);
        $this->handler = new CreateActivityHandler($this->activityRepository);
    }

    public function testHandle(): void
    {
        $activity = $this->activityFakeBuilder->generate();

        $command = new CreateActivityCommand(
            $activity->getIdentifier(),
            $activity->getName(),
            $activity->getPosition(),
            $activity->getDifficulty(),
            $activity->getTime(),
            $activity->getSolution(),
            $activity->getItinerary()
        );

        $this->activityRepository->expects($this->once())
            ->method('createOrFail')
            ->with($activity);

        $this->handler->handle($command);
    }


}
