<?php

namespace App\Command;

use App\Entity\Answer;
use App\Repository\ActivityRepository;
use App\Repository\AnswerRepository;
use App\Service\AnswerScoreCalculator;
use App\Tests\FakeBuilder\ActivityFakeBuilder;
use Carbon\Carbon;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class RegisterActivityHandlerTest extends TestCase
{
    private ActivityFakeBuilder $activityFakeBuilder;
    private RegisterActivityHandler $handler;
    private MockObject $activityRepository;
    private MockObject $answerRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->activityFakeBuilder = new ActivityFakeBuilder();
        $this->activityRepository = $this->createMock(ActivityRepository::class);
        $this->answerRepository = $this->createMock(AnswerRepository::class);
        $this->handler = new RegisterActivityHandler($this->activityRepository, $this->answerRepository, new AnswerScoreCalculator());
    }

    public function testHandle(): void
    {
        Carbon::setTestNow('2022-05-08');

        $student = 1;
        $activity = $this->activityFakeBuilder
            ->withIdentifier('A1')
            ->withPosition(2)
            ->withSolution('1_0_2')
            ->withItinerary(1)
            ->generate();

        $answer = new Answer($student, 'A1', 66, 100, '1_0_1');

        $command = new RegisterActivityCommand($answer->getActivityIdentifier(), $answer->getSolution(), $answer->getTime(), $student);

        $this->activityRepository->expects($this->once())
            ->method('findByIdentifier')
            ->willReturn($activity);

        $this->answerRepository->expects($this->once())
            ->method('create')
            ->with($answer);

        $this->handler->handle($command);
    }

    public function testHandleWithInvalidSolutionThrowsException(): void
    {
        $student = 1;
        $activity = $this->activityFakeBuilder
            ->withIdentifier('A1')
            ->withPosition(2)
            ->withSolution('1_0_2')
            ->withItinerary(1)
            ->generate();

        $answer = new Answer($student, 'A1', 66, 100, '1_0_1_3');

        $command = new RegisterActivityCommand($answer->getActivityIdentifier(), $answer->getSolution(), $answer->getTime(), $student);

        $this->activityRepository->expects($this->once())
            ->method('findByIdentifier')
            ->willReturn($activity);

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Given answers "1_0_1_3" does not match with solution "1_0_2"');


        $this->handler->handle($command);
    }
}
