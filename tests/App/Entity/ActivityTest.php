<?php

namespace App\Entity;

use App\Tests\FakeBuilder\ActivityFakeBuilder;
use Exception;
use PHPUnit\Framework\TestCase;

class ActivityTest extends TestCase
{
    private ActivityFakeBuilder $activityFakeBuilder;

    protected function setUp(): void
    {
        parent::setUp();
        $this->activityFakeBuilder = new ActivityFakeBuilder();
    }

    public function testCreate(): void
    {
        $activity = $this->activityFakeBuilder
            ->withIdentifier('ID')
            ->withName('Name')
            ->withPosition(2)
            ->withDifficulty(7)
            ->withTime(100)
            ->withSolution('1_0_2')
            ->withItinerary(1)
            ->generate();

        $this->assertInstanceOf(Activity::class, $activity);
        $this->assertEquals('ID', $activity->getIdentifier());
        $this->assertEquals('Name', $activity->getName());
        $this->assertEquals(2, $activity->getPosition());
        $this->assertEquals(7, $activity->getDifficulty());
        $this->assertEquals(100, $activity->getTime());
        $this->assertEquals('1_0_2', $activity->getSolution());
        $this->assertEquals(1, $activity->getItinerary());
    }

    public function testCreateWithInvalidDifficultyThrowException(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Invalid value of difficulty "15"');

        $activity = $this->activityFakeBuilder
            ->withDifficulty(15)
            ->generate();
    }


}
