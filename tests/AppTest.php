<?php

declare(strict_types=1);

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AppTest extends DatabaseTestCase
{
    public function test(): void
    {
        $this->assertTrue(true);
    }

    public function testAddActivity()
    {
        // This calls KernelTestCase::bootKernel(), and creates a
        // "client" that is acting as the browser
        $client = self::createClient();

        $client->request('GET', '/activities/itinerary/1');

        $response = $client->getResponse();

        // Validate a successful response and some content
        $this->assertResponseIsSuccessful();
    }
}
